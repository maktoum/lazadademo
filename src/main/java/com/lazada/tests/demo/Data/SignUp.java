package com.lazada.tests.demo.Data;

/**
 * Created by mohammedy on 11/4/14.
 */
public class SignUp {

    public String gender = null;
    public String email = null;
    public String name = null;
    public String password = null;
    public String confirmPassword = null;
    public String blankPasswordErrorMessage = null;


    public SignUp(String randomNo){
        gender = "Male";
        email = randomNo + "@lazadademo.com";
        name = randomNo;
        password = randomNo;
        confirmPassword  =randomNo;
        blankPasswordErrorMessage ="Password must contain at least one number";

    }
}
