package com.lazada.tests.demo.pageobjects;

import com.lazada.tests.demo.Data.Product;
import com.lazada.tests.demo.utils.SeleniumBase;
import com.thoughtworks.selenium.condition.Condition;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

/**
 * Created by mohammedy on 11/5/14.
 */
public class CartPage extends SeleniumBase{

    By trPoducts = By.xpath("//*[@class='width_100 producttable']/tbody/tr");
    By btnClose = By.xpath("//a[@class='nyroModalClose nyroModalCloseButton nmReposition']");

    public CartPage(WebDriver driver){
        webDriver =driver;
    }

    public Product getProductDetails(Product product){
        Product actualProductDetails = new Product();
        List<WebElement> products = getElements(trPoducts);
        WebElement productDescription = null;
        WebElement productPrice = null;

        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        WebElement element = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//td/select[@title='Quantity']")));


        for (int row = 0;  row < products.size();row ++){
            productDescription = products.get(row).findElement(By.xpath("./td/div[@class='productdescription']"));
            productPrice = products.get(row).findElement(By.xpath("./td[contains(@class,'price center')]/span"));
            if(productDescription.getText().equalsIgnoreCase(product.name)){
                actualProductDetails.name = productDescription.getText();
                actualProductDetails.price = productPrice.getText();
            }
        }
        return actualProductDetails;
    }

    public Product changeQty(Product product){
        Product actualProductDetails = new Product();
        List<WebElement> products = getElements(trPoducts);
        WebElement productDescription = null;
        WebElement qty = null;

        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        WebElement element = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//td/select[@title='Quantity']")));


        for (int row = 0;  row < products.size();row ++){
            productDescription = products.get(row).findElement(By.xpath("./td/div[@class='productdescription']"));
            qty = products.get(row).findElement(By.xpath("./td/select[@title='Quantity']"));
            if(productDescription.getText().equalsIgnoreCase(product.name)){
                selectByText(qty, product.qty);
            }
        }

        element = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Cart updated']")));


        return actualProductDetails;
    }

    public String getSubTotal(){
        By subTotal = By.xpath("//div[@id='subtotal']/table/tbody/tr[@class='sub']/td[@class='width_30 right_align lastcolumn']/div");

        return getText(subTotal);
    }

    public CartPage closeCartWindow(){
        click(btnClose);
        return this;
    }
}
