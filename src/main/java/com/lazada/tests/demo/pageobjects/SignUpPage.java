package com.lazada.tests.demo.pageobjects;

import com.lazada.tests.demo.Data.SignUp;
import com.lazada.tests.demo.utils.SeleniumBase;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

/**
 * Created by mohammedy on 11/4/14.
 */
public class SignUpPage extends SeleniumBase {

    By drpGender = By.id("RegistrationForm_gender");
    By txtEmail = By.id("RegistrationForm_email");
    By txtName = By.id("RegistrationForm_first_name");
    By txtPassword = By.id("RegistrationForm_password");
    By txtConfirmPassword = By.id("RegistrationForm_password2");
    By btnSubmit = By.id("send");
    By lblConfirmPassword = By.xpath("//input[@id='RegistrationForm_password2']/following::div");

    public SignUpPage(WebDriver driver){
         webDriver = driver;
    }

    public  SignUpPage fillCustomerCreationForm(SignUp signUp){

        selectByText(drpGender, signUp.gender);
        type(txtEmail, signUp.email);
        type(txtName, signUp.name);
        type(txtPassword, signUp.password);
        type(txtConfirmPassword, signUp.confirmPassword);
      ((JavascriptExecutor)webDriver).executeScript("arguments[0].scrollIntoView();"
                                                            ,getElement(btnSubmit));
      
        click(btnSubmit);

        return this;
    }

    public String getBlankPasswordErrorMessage(){
        return getText(lblConfirmPassword);
    }
}
