package com.lazada.tests.demo.pageobjects;

import com.lazada.tests.demo.utils.SeleniumBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by mohammedy on 11/5/14.
 */
public class SearchResultPage extends SeleniumBase {

    By searchResultsImages = By.xpath("//img[@class='state_loaded']");

    public SearchResultPage(WebDriver driver){
        webDriver = driver;
    }

    public SearchResultPage loadProductDetailsPage(int productIndext){
        List<WebElement> products = getElements(searchResultsImages);
        click(products.get(productIndext));
        return   this;
    }

}
