package com.lazada.tests.demo.pageobjects;

import com.lazada.tests.demo.Data.Product;
import com.lazada.tests.demo.utils.SeleniumBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by mohammedy on 11/5/14.
 */
public class ProductDetailsPage extends SeleniumBase {

    By lblName = By.id("prod_title");
    By lblPrice = By.id("special_price_box");
    By lblDeliveryDays = By.xpath("//span[@class='deliveryTime strong']");
    By btnAddCart = By.id("AddToCart");

    public ProductDetailsPage(WebDriver driver){
        webDriver = driver;
    }

    public Product getProduct(){
        Product product = new Product();
        product.name = getText(lblName);
        product.price = getText(lblPrice);
        product.deliveryDays = getText(lblDeliveryDays);
        return product;
    }

    public ProductDetailsPage addToCart(){
        click(btnAddCart);
        return this;
    }
}
