package com.lazada.tests.demo.pageobjects;

import com.lazada.tests.demo.Data.SignUp;
import com.lazada.tests.demo.utils.SeleniumBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by mohammedy on 11/4/14.
 */
public class HomePage extends SeleniumBase {

    By lnkUserAccount = By.xpath("//li[@class='header__user__account header__navigation__item dropdown__container header__navigation__item-arrow']/span");
    By lnkSignUp = By.id("sign-up-now");
    By lnkHighLightsAtLazada = By.linkText("Highlights at Lazada");
    By txtSearch = By.id("searchInput");
    By btnSearch = By.xpath("//div[@class='header__search__submit']");

    public HomePage(WebDriver driver){
         webDriver = driver;
    }

    public HomePage loadSignUpPage(){
        click(lnkUserAccount);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        WebElement element = wait.until(
                ExpectedConditions.visibilityOfElementLocated(lnkSignUp));

        click(lnkSignUp);
        return this;
    }

    public HomePage searchProducts(String text){
        type(txtSearch, text);
        click(btnSearch);
        return this;
    }


}
