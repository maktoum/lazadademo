package com.lazada.tests.demo.utils;

import org.testng.annotations.*;
import org.openqa.selenium.WebDriver;

/**
 * Created by mohammedy on 11/4/14.
 */
public class TestBase {

    protected static String baseUrl = System.getProperty("sys.base.url","http://www.lazada.sg/");
    protected static String browser = System.getProperty("web.browser","Firefox");
    protected static WebDriver driver = null;
    protected static DriverFactory driverFactory = null;

    @BeforeClass
    public static void setUp(){
        driverFactory = new DriverFactory();
        driver = driverFactory.getWebDriver(browser);
        driver.navigate().to(baseUrl);
    }


}
