package com.lazada.tests.demo.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by mohammedy on 11/4/14.
 */
public class SeleniumBase {

   protected static WebDriver webDriver = null;

    public SeleniumBase(){
        
    }
   public SeleniumBase(WebDriver driver){
       this.webDriver = driver;
   }

    public void type(By by, String text){
        WebElement element = webDriver.findElement(by);
        //((JavascriptExecutor)webDriver).executeScript("arguments[0].scrollIntoView();"
          //      ,element);

        element.sendKeys(text);
    }

    public void click (By by){
        WebElement element = webDriver.findElement(by);
        //((JavascriptExecutor)webDriver).executeScript("arguments[0].scrollIntoView();"
          //                                                    ,element);
        element.click();
    }
    public void click (WebElement element){
    	//((JavascriptExecutor)webDriver).executeScript("arguments[0].scrollIntoView();"
          //      ,element);

        element.click();
    }

    public void selectByText (By by, String text){
        if(!text.equals(""))
            new Select(webDriver.findElement(by)).selectByVisibleText(text);
    }


    public void selectByText (WebElement element, String text){
        if(!text.equals(""))
            new Select(element).selectByVisibleText(text);
    }

    public String getText (By by){
        WebElement element = webDriver.findElement(by);
        return element.getText();
    }

    public WebElement getElement(By by){
        return webDriver.findElement(by);
    }

    public List<WebElement> getElements(By by){
        return webDriver.findElements(by);
    }
}
