package com.lazaca.tests.demo.scripts;

import com.lazada.tests.demo.Data.Product;
import com.lazada.tests.demo.Data.SignUp;
import com.lazada.tests.demo.pageobjects.*;
import com.lazada.tests.demo.utils.TestBase;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.UUID;

/**
 * Created by mohammedy on 11/4/14.
 */
public class ProductSubTotalDemoTest extends TestBase{

    static SignUpPage signUpPage = null;
    static SignUp signUp = null;
    static String uniqueId = null;

    static HomePage homePage = null;
    static SearchResultPage searchResultPage = null;
    static Product product = null;
    static ProductDetailsPage productDetailsPage = null;
    static CartPage cartPage = null;

    @BeforeClass
    public static void init(){
        homePage = new HomePage(driver);
        searchResultPage = new SearchResultPage(driver);
        productDetailsPage = new ProductDetailsPage(driver);
        product = new Product();
        cartPage = new CartPage(driver);
        signUpPage = new SignUpPage(driver);
        uniqueId = UUID.randomUUID().toString().split("-")[0];
        signUp = new SignUp(uniqueId);

    }

    @AfterClass
    public static void tearDown(){
        driver.close();
    }

  
    @Test
    public void testSubTotalPrice() throws InterruptedException {
        Product firstProduct  = new Product();
        Product secondProcuct = new Product();
        
        driver.navigate().refresh();
        homePage.searchProducts("mobile");
        searchResultPage.loadProductDetailsPage(0);
        firstProduct = productDetailsPage.getProduct();
        productDetailsPage.addToCart();
        cartPage.closeCartWindow();
        driver.navigate().back();
        searchResultPage.loadProductDetailsPage(1);
        secondProcuct = productDetailsPage.getProduct();
        productDetailsPage.addToCart();

        firstProduct.qty = "2";
        secondProcuct.qty = "1";

        cartPage.changeQty(firstProduct);
        String actualSubTotal = cartPage.getSubTotal().split(" ")[1].trim();
        Product [] products = {firstProduct, secondProcuct};
        Assert.assertEquals(actualSubTotal, getSubTotal(products));

    }

    


    public String getSubTotal (Product [] products){
        double subTotal = 0;
        NumberFormat formatter = new DecimalFormat("#0.00");
        for (int index = 0; index < products.length;   index ++){
            int qty = Integer.parseInt(products[index].qty);
            double price = Double.parseDouble(products[index].price.split(" ")[1].trim());
            subTotal += qty*price;
        }

        return String.valueOf(formatter.format(subTotal));
    }


}
